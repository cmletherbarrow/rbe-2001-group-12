/*
 * EStop.h
 *	contains an interface function for the Messages class to call
 *
 *  Created on: Feb 26, 2018
 *      Author: Lingrui
 */

#ifndef ESTOP_H_
#define ESTOP_H_

void eStop();

#endif /* ESTOP_H_ */
