/*
 * Messages.cpp
 *
 *  Created on: Sep 15, 2016
 *      Author: bradmiller
 */
#include "Arduino.h"
#include "Messages.h"
#include "BTComms.h"
#include "EStop.h" //for the emergency stop function

BTComms comms;

/**
 * Constuctor
 * Initialize everything here when the class is created
 * Note: you cannot call methods that depend on other classes having already been created
 */
Messages::Messages() {
  movementstatus = movementStopped; 
  gripperstatus = gripperEmpty;
  opstatus = opsAttempt;
	stopped = false;
  hasConnected = false; 
}

/**
 * Setup class code that is called from the Arduino sketch setup() function. This doesn't
 * get called until all the other classes have been created.
 */
void Messages::setup() {
	comms.setup();
}

/**
 * Check if the field is currently in the "stop" state
 * @returns bool value that is true if the robot should be stopped
 */
bool Messages::isStopped() {
	return stopped;
 
}

/**
 * Send a heartbeat message to the field to let it know that your code is alive
 * This should be called by your robot program periodically, say once per second. This
 * timing can easily be done in the loop() function of your program.
 */
void Messages::sendHeartbeat() {
	comms.writeMessage(kHeartbeat, 0x0a, 0x00);
}
//when the robot is containing a fuel rod, send radiation alert, only send once per second
void Messages::sendSpentAlert(){
  comms.writeMessage(kRadiationAlert, fuelRodSpent, 0x0c, 0x00);
  Serial.println("Sending Spent Alert");
}
void Messages::sendNewAlert(){
  comms.writeMessage(kRadiationAlert, fuelRodNew, 0x0c, 0x00);
  Serial.println("Sending New Alert");
}
void Messages::sendRobotStatus(){
  comms.writeMessage(kRobotStatus, movementstatus, gripperstatus, opstatus, 0x0c, 0x00);
  Serial.println("Sending Robot Status");
}
/**
 * Print message for debugging
 * This method prints the message as a string of hex numbers strictly for debugging
 * purposes and is not required to be called for any other purpose.
 */
void Messages::printMessage() {
    for (int i = 0; i < comms.getMessageLength(); i++) {
      Serial.print(comms.getMessageByte(i), HEX);
      Serial.print(" ");
    }
    Serial.println();
}

/**
 * Read messages from the Bluetooth serial connection
 * This method should be called from the loop() function in your arduino code. It will check
 * to see if the lower level comms object has received a complete message, and run the appropriate
 * code to handle the message type. This should just save the state of the message inside this class
 * inside member variables. Then add getters/setters to retrieve the status from your program.
 */
bool Messages::read() {
  if (comms.read()) {
    hasConnected = true;
    switch (comms.getMessageByte(0)) {
    case kStorageAvailability:
    storageAvailability[0] = comms.getMessageByte(3) & 1;
    storageAvailability[1] = comms.getMessageByte(3) & 2;
    storageAvailability[2] = comms.getMessageByte(3) & 4;
    storageAvailability[3] = comms.getMessageByte(3) & 8;   
    Serial.println("Storage Data Received");
      break;
    case kSupplyAvailability:
    supplyAvailability[0] = comms.getMessageByte(3) & 1;
    supplyAvailability[1] = comms.getMessageByte(3) & 2;
    supplyAvailability[2] = comms.getMessageByte(3) & 4;
    supplyAvailability[3] = comms.getMessageByte(3) & 8;  
    Serial.println("Supply Data Received");
      break;
    case kRadiationAlert:
    //not used, don't need to receive radalert
      break;
    case kStopMovement:
      stopped = true;
      eStop();
      Serial.println("Stop Movement");
      break;
    case kResumeMovement:
      stopped = false;
      Serial.println("Resume Movement");
      break;
    case kRobotStatus:
    //not used, don't need to recieve robot status
      break;
    case kHeartbeat:
    //not used, don't need to recieve heartbeat
      break;
    }
    return true;
  }
  return false;
}
