#### What to do when Arduino.h not found ###
- check if the two things that sloeber asks to check off is actually off (it is per workspace) 
	- windows -> preferences -> C/C++ -> Indexer	
		uncheck "index source files not included in the build" && "index unused headers"
- check if the board is properly set (see if the board is right next to the project name)
	- right click the project -> Properties -> Arduino
		"Platform folder" should have something now, and select that one option
		Set "Board" and "Processor" should have something how. 
		Set "Port" once the board is connected
		