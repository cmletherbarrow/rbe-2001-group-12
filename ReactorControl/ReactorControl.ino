/*
 * ReactorControl.ino
 *	The main Arduino program for the RBE2001 final project
 *
 *  Created on: Feb 17, 2018
 *      Author: Lingrui (Eula), Christopher Letherbarrow, Bintang Hornbuckle
 */

#include "Arduino.h"
#include "Robot.h"
#include "Messages.h"
#include "EStop.h"
#include "L3G.h"
#include "Wire.h"
#include "Servo.h"
#include "Encoder.h"

#define FRAME (25) // gyro heading refresh rate
#define MOTOR_STOP (96) //motor stop input
//==================================================
//===                   GYRO                     === 
//==================================================          

L3G gyro;

//stores the current accumulated heading read by the current frame
double heading;
int zero;

// turn off gyro reading when not needed to reduce interference with other sensor that interrupts
bool usegyro;

/**
 * reset the accumulated heading to zero and get ready for the next read
 */
void zeroHeading() {
	heading = 0.0;
	usegyro = true;
	Serial.println("Resetting");
}

/**
 * should only be called once in the loop() function for every frame
 */
double getHeading() {
	gyro.read();

	//integrates the gyro reading using the time interval since last read
	//assuming the robot is turning at a constant angular velocity since the last reading
	//unit in millis degrees (mdegree/s * ms * 1s/1000ms)
	//positive = CW
	if (((int) gyro.g.y > 0 ? (int) gyro.g.y : -gyro.g.y) > 500) {
		heading += ((double) gyro.g.y - zero) * FRAME * 0.00000875; //8.75 / 1000 / 1000;
//    lastread = millis();
	}
	return heading;
}

//==================================================
//===               ARM MOTORS                   === 
//================================================== 
static int rackmotorpin = 9;
Servo rackmotor;
static int encoderpinF = 2;
static int encoderpinB = 3;
Encoder rackenc(encoderpinF, encoderpinB);
//numbers of encoder tick for the rack to extend and retrieve in horizontal orientation
const int tickH = 900;
//for vertical
const int tickV = 600;
//325 ticks per rev

/**
 * resets the encoder count
 */
void startrack() {
	rackenc.write(0);
	delay(100);
}

/**
 * ticks: should either be tickH or tickV
 */
bool retractRack(int ticks) {
	Serial.print("Rack: ");
	Serial.println(rackenc.read());
	if (ticks < abs(rackenc.read())) {
		rackmotor.write(MOTOR_STOP);
		return true;
	}
	rackmotor.write(0);
	return false;
}

/**
 * ticks: should either be tickH or tickV
 */
bool extendRack(int ticks) {
	Serial.print("Rack: ");
	Serial.println(rackenc.read());
	if (ticks < abs(rackenc.read())) {
		rackmotor.write(MOTOR_STOP);
		return true;
	}
	rackmotor.write(180);
	return false;
}

//==================================================
//===                 GRIPPER                    === 
//================================================== 
static int gripperpin = 10;
Servo grippermotor;
unsigned long gripperstarttime;
const int duration = 300;

//the exact stop input is somewhere between 95 and 96.
//if either of the value is used for stopping the gripper, the gripper
//would either slowly open or close
const int open_stop = 95;
const int close_stop = 96;

void startgripper() {
	gripperstarttime = millis();
}

bool closeGripper() {
	if (millis() - gripperstarttime > duration) {
		grippermotor.write(close_stop);
		return true;
	}
	grippermotor.write(180);
	return false;
}

bool openGripper() {
	if (millis() - gripperstarttime > duration) {
		grippermotor.write(open_stop);
		return true;
	}
	grippermotor.write(0);
	return false;
}

//==================================================
//===             STATE MACHINE VARS             ===
//==================================================  

Messages msg;

/**
 * large state of the autonomous program
 */
typedef enum {
	PickingRod, AToStorage, BToStorge, StorageToSupply, SupplyToReactor, AToB
} largeState;

largeState largestate;

//each corresponds to a function in Robot.h
//records the state of the function call within a large state
//without blocking the program
typedef enum {
	Raise4Bar,
	Lower4Bar,
	DriveTillLine,
	TurnCCW90,
	TurnCW90,
	Turn180,
	DriveTime,
	DriveTillHit,
	SetUp,
	ExtendRackV,
	ExtendRackH,
	RetractRackV,
	RetractRackH,
	PushBackRetract,
	PushBackExtend,
	OpenGripper,
	CloseGripper,
	BackUpToLine,
	BackUpToCenter,
	BackUpTime,
	Choice
} smallState;

smallState smallstate;

//whether the robot possesses a new fuel rod
bool haveNewRod = false;

//robot possesses a spent fuel rod
bool haveSpentRod = false;

/**
 * number of lines to go pass when the robot goes from the reactor to the storage tubes
 * set by findClosestStorageA/B()
 */
int linesToGoAtoST;
int linesToGoBtoST;

//which storage tube did the robot go to; set by findClosestStorageA/B()
int ST;

/**
 * the 3 different possible ways to go from a storage slot to a supply slot depending on the availability
 */
enum {
	straight, left, right
} ST2SPtype; //0 = straight across; 1 = turn left; 2 = turn right;

//how many lines to go after turning left or right from storage to supply
int linetogo;

//for mirrored states to determine what is the next state to go to
bool isA;

//contains functions that specifically controls the robot except for the rack, the gripper, and the gyro
Robot robot;

//pin 20 is for i2c
const int eButtonPin = 23;

//last time the heartbeat is sent
int lastBT;

//==================================================
//===         STATE MACHINE CALCULATIONS         ===
//==================================================
/**
 * closest to farthest from B: 1 2 3 4
 */
void findClosestStorageB() {
	for (int i = 0; i <= 3; i++) {
		if (msg.storageAvailability[i] == false) {
			linesToGoBtoST = i + 1;
			ST = i;
			break;
		}
	}
}

/**
 * closest to farthest from B: 4 3 2 1
 */
void findClosestStorageA() {
	for (int i = 3; i >= 0; i--) {
		if (msg.storageAvailability[i] == false) {
			linesToGoAtoST = 4 - i;
			ST = i;
			break;
		}
	}
}

/**
 * st = [0, 3]
 */
void closestSupplyFromStorage(int st) {
	//(3-st) gets the index of the supply tube across of the given storage index
	int opposite = 3 - st;
	//true means available
	if (msg.supplyAvailability[opposite] == true) {
		ST2SPtype = straight;
		linetogo = 0;
		return;
	}
	for (int x = 0; x < 4; x++) {
		if (msg.supplyAvailability[x]) {
			if (opposite < x) {
				ST2SPtype = right;
				linetogo = x - opposite;
			} else {
				ST2SPtype = left;
				linetogo = opposite - x;
			}
			return;
		}
	}
}

//==================================================
//===           STATE MACHINE STATES             ===
//==================================================

void pickingRod() {
	switch (smallstate) {
	case SetUp:
		startgripper();
		smallstate = OpenGripper;
		Serial.println("SetUp");
		break;
	case OpenGripper:
		if (openGripper()) {
			startrack();
			smallstate = ExtendRackV;
			msg.opstatus = msg.opsAttempt;
			Serial.println("opengripper");
		}
		break;
	case ExtendRackV:
		if (extendRack(tickV)) {
			startgripper();
			smallstate = CloseGripper;
			Serial.println("extendrackv");
		}
		break;
	case CloseGripper:
		if (closeGripper()) {
			startrack();
			smallstate = RetractRackV;
			Serial.println("closegripper");
			haveSpentRod = true;
			msg.gripperstatus = msg.gripperFull;
			robot.ledOn();
		}
		break;
	case RetractRackV:
		if (retractRack(tickV)) {
			smallstate = Lower4Bar;
			robot.resetVals();
			Serial.println("retract");
		}
		break;
	case Lower4Bar:
		if (robot.lower4Bar()) {
			smallstate = BackUpTime;
			Serial.println("lower4bar");
			robot.resetVals();
		}
		break;
	case BackUpTime:
		if (robot.driveTime(175, false, 700)) { //drive in 175 speed in reverse direction for 700 ms
			robot.resetVals();
			smallstate = Turn180;
			zeroHeading();
			Serial.println("backup");
		}
		break;
	case Turn180:
		//check isstop
		if (robot.turn180CCW(200, heading)) {
			largestate = AToStorage;
			smallstate = SetUp;
			Serial.println("turnaround");
			usegyro = false;
		}
		break;
	}

}

void reactorToStorage() {
	switch (smallstate) {
	case SetUp:
		if (isA) {
			findClosestStorageA();
		} else {
			findClosestStorageB();
		}
		smallstate = DriveTillLine;
		Serial.print("Going to ");
		Serial.print(linesToGoAtoST);
		Serial.println("setup");
		msg.opstatus = msg.opsToStorage;
		robot.resetVals();
		break;
	case DriveTillLine:
		if (robot.driveTillLine(isA ? linesToGoAtoST : linesToGoBtoST, 200,
				true)) {
			smallstate = BackUpToLine;
			delay(500); //stop for a bit for the robot to stabilize
			Serial.println("drivetillstorage");
			robot.resetVals();
		}
		break;
	case BackUpToLine:
		if (robot.driveTillLine(1, 200, false)) {
			Serial.println("backed up a bit");
			zeroHeading();
			if (isA) {
				smallstate = TurnCCW90;
			} else {
				smallstate = TurnCW90;
			}
		}
		break;
	case TurnCCW90:
		if (robot.turn90CCW(150, heading)) {
			Serial.println("turn ccw 90");
			smallstate = DriveTillHit;
			usegyro = false;
		}
		break;
	case TurnCW90:
		if (robot.turn90CW(150, heading)) {
			Serial.println("turn ccw 90");
			smallstate = DriveTillHit;
			usegyro = false;
		}
		break;
	case DriveTillHit:
		if (robot.driveTillHit(255)) {
			Serial.println("drive to slot");
			startrack();
			smallstate = ExtendRackH;
		}
		break;
	case ExtendRackH:
		if (extendRack(tickH)) {

			Serial.println("extended");
			smallstate = OpenGripper;
			msg.opstatus = msg.opsRelease;
			startgripper();
		}
		break;
	case OpenGripper:
		if (openGripper()) {
			Serial.println("gripper open");
			startrack();
			haveSpentRod = false;
			msg.gripperstatus = msg.gripperEmpty;
			robot.ledOFF();
			smallstate = RetractRackH;
			robot.resetVals();
		}
		break;
	case RetractRackH:
		if (retractRack(tickH)) {
			smallstate = BackUpToCenter;
			robot.resetVals();
		}
		break;
	case BackUpToCenter:
		Serial.println("back");
		if (robot.driveTillLine(1, 150, false)) {
			zeroHeading();
			largestate = StorageToSupply;
			smallstate = SetUp;
		}
		break;
	}
}

smallState tinystate;

bool STSright() {
	switch (tinystate) {
	case TurnCW90:
		Serial.println("right, turn cw 90");
		if (robot.turn90CW(200, heading)) {
			robot.resetVals();
			tinystate = DriveTillLine;
			usegyro = false;
		}
		break;
	case DriveTillLine:
		if (robot.driveTillLine(linetogo, 200, true)) {
			robot.resetVals();
			return true;
		}
		break;
	}
	return false;
}

bool STSleft() {
	switch (tinystate) {
	case TurnCCW90:
		Serial.println("left, turn ccw 90");
		if (robot.turn90CCW(200, heading)) {
			robot.resetVals();
			tinystate = DriveTillLine;
			usegyro = false;
		}
		break;
	case DriveTillLine:
		Serial.print(linetogo);
		Serial.println(" DriveTillLine");
		if (robot.driveTillLine(linetogo, 200, true)) {
			robot.resetVals();
			return true;
		}
		break;
	}
	return false;
}

void storageToSupply() {
	switch (smallstate) {
	case SetUp:
		Serial.println("calculating sp to st");
		closestSupplyFromStorage(ST);
		Serial.print(ST);
		Serial.print(" ");
		Serial.print(ST2SPtype);
		Serial.print(" ");
		Serial.println(linetogo);
		smallstate = Choice;
		msg.opstatus = msg.opsToSupply;
		if (ST2SPtype == left) {
			tinystate = TurnCCW90; //trying to call the same function twice in one largestate
		} else if (ST2SPtype == right) {
			tinystate = TurnCW90; //trying to call the same function twice in one largestate
		}
		zeroHeading();
		break;
	case Choice:
		switch (ST2SPtype) {
		case straight:
			Serial.println("Straight");
			if (robot.turn180CCW(150, heading)) {
				usegyro = false;
				smallstate = DriveTillHit;
				robot.resetVals();
			}
			break;
		case left:
			Serial.println("Left");
			if (STSleft()) { //using another tiny state as part of the state machine
				zeroHeading();
				smallstate = TurnCCW90;
			}
			break;
		case right:
			Serial.println("Right");
			if (STSright()) {
				zeroHeading();
				smallstate = TurnCW90;
			}
			break;
		}
		break;
	case TurnCW90:
		if (robot.turn90CW(200, heading)) {
			usegyro = false;
			smallstate = DriveTillHit;
			robot.resetVals();
		}
		break;
	case TurnCCW90:
		if (robot.turn90CCW(200, heading)) {
			usegyro = false;
			smallstate = DriveTillHit;
			robot.resetVals();
		}
		break;
	case DriveTillHit:
		if (robot.driveTillHit(255)) {
			Serial.println("drive to slot");
			startrack();
			startgripper();
			smallstate = OpenGripper;
		}
		break;
	case OpenGripper:
		if (openGripper()) {
			startrack();
			smallstate = ExtendRackH;
			msg.opstatus = msg.opsRelease;
			Serial.println("opengripper");
		}
		break;
	case ExtendRackH:
		if (extendRack(tickH) && openGripper()) {
			Serial.println("extended");
			startgripper();
			startrack();
			smallstate = CloseGripper;
		}
		break;
	case CloseGripper:
		if (closeGripper()) {
			startrack();
			smallstate = RetractRackH;
			Serial.println("closegripper");
			haveNewRod = true;
			robot.ledOn();
		}
		break;
	case RetractRackH:
		if (retractRack(tickH)) {
			smallstate = BackUpToCenter;
			robot.resetVals();
		}
		break;
	case BackUpToCenter:
		Serial.println("back");
		if (robot.driveTillLine(1, 150, false)) {
			largestate = SupplyToReactor;
			smallstate = SetUp;
		}
		break;
	}
}

void supplyToReactor() {
	switch (smallstate) {
	case SetUp:
		zeroHeading();
		if (isA) {
			smallstate = TurnCW90;
		} else {
			smallstate = TurnCCW90;
		}
		msg.opstatus = msg.opsToReactor;
		break;
	case TurnCW90:
		if (robot.turn90CW(200, heading)) {
			robot.resetVals();
			smallstate = DriveTillHit;
		}
		break;
	case TurnCCW90:
		if (robot.turn90CCW(200, heading)) {
			robot.resetVals();
			smallstate = DriveTillHit;
		}
		break;
	case DriveTillHit:
		if (robot.driveTillHit(255)) {
			smallstate = Raise4Bar;
		}
		break;
	case Raise4Bar:
		if (robot.raise4Bar()) {
			startrack();
			smallstate = ExtendRackV;
		}
		break;
	case ExtendRackV:
		if (extendRack(tickV)) {
			startgripper();
			smallstate = OpenGripper;
		}
		break;
	case OpenGripper:
		if (openGripper()) {
			startrack();
			smallstate = RetractRackV;
			robot.ledOFF();
			haveNewRod = false;
		}
		break;
	case RetractRackV:
		if (retractRack(tickV)) {
			robot.resetVals();
			smallstate = BackUpTime;
		}
		break;
	case BackUpTime:
		if (robot.driveTime(175, false, 700)) {
			robot.resetVals();
			smallstate = Turn180;
			zeroHeading();
			Serial.println("backup");
		}
		break;
	case Turn180:
		if (robot.turn180CCW(200, heading)) {
			Serial.println("turnaround");
			usegyro = false;
			if (isA) {
				isA = false;
				largestate = AToB;
				smallstate = SetUp;
			} else {
				delay(200000000);
			}
		}
		break;
	}
}

void aToB() {
	switch (smallstate) {
	case SetUp:
		smallstate = DriveTillHit;
		delay(500);
		break;
	case DriveTillHit:
		if (robot.driveTillHit(255)) {
			largestate = PickingRod;
			smallstate = SetUp;
		}
		break;
	}
}

void eStop() {
	if (msg.stopped == true) {
		msg.stopped = false;
	} else {
		msg.stopped = true;
		robot.botStop();
	}
}

//==================================================
//===                   MAIN                     === 
//==================================================  

void setup() {
	Serial.begin(115200);
	//GYRO
	Wire.begin();
	msg.movementstatus = msg.movementActive;

	//keeps polling the gyro status
	//side effect: blocks the program till the power is turned on
	while (!gyro.init()) {
		Serial.println("Failed to autodetect gyro type!");
		delay(200);
	}
	gyro.enableDefault();

	//GYRO calibration
	double calibration = 0;
	for (int x = 0; x < 100; x++) {
		gyro.read();
		calibration += gyro.g.y;
	}
	zero = calibration / 100.0;

	//4 bar related init
	grippermotor.attach(gripperpin);
	rackmotor.attach(rackmotorpin);
	rackmotor.write(MOTOR_STOP);
	grippermotor.write(MOTOR_STOP);

	gripperstarttime = millis();

	//STATE MACHINE init
	largestate = PickingRod;
	smallstate = SetUp;
	isA = true;
	Serial.println("Starting");
	usegyro = false;

	//initiate bluetooth message frequency counter
	lastBT = millis();

	msg.setup();

	//didn't get to implement since the interrupt interferes with gyro;
//	pinMode(eButtonPin, INPUT_PULLUP);
//	attachInterrupt(digitalPinToInterrupt(eButtonPin), eStop, FALLING);
}

//states for recording the frame rate
int ltime = 0;
int curtime = 0;

// The loop function is called in an endless loop
void loop() {
	curtime = millis();

	switch (largestate) {
	case PickingRod:
		pickingRod();
		break;
	case AToStorage:
		reactorToStorage();
		break;
	case StorageToSupply:
		storageToSupply();
		break;
	case SupplyToReactor:
		supplyToReactor();
		break;
	case AToB:
		aToB();
		break;
	}

	//send BT messages
	if (millis() - lastBT >= 1000) {
		msg.sendHeartbeat();
		if (haveNewRod) {
			msg.sendNewAlert();
		}
		if (haveSpentRod) {
			msg.sendSpentAlert();
		}
		msg.sendRobotStatus();
		lastBT = millis();
	}
	msg.read();

	//set frame rate for gyro and everything else
	while (millis() - ltime < FRAME) {
	}
	//the gyro can not be used simultaneously with bluetooth
	if (usegyro) {
		Serial.print("Heading: ");
		Serial.println(getHeading());
	}
	ltime = millis();
}

