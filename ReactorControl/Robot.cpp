/*
 * Robot.cpp
 *	The implementation on controlling different parts of the robot;
 *	contain reference to all electronic parts except for the gripper, the rack, and the gyro
 *
 *  Created on: Feb 24, 2018
 *      Author: Lingrui
 */

#include "Robot.h"
#include "Arduino.h"

//how low is dark
#define LIGHT_THRESHOLD (285)

static int rmotorpinF = 8; //forward pin
static int rmotorpinB = 11; //backward pin

static int lmotorpinF = 6; //forward pin
static int lmotorpinB = 7; //backward pin

static int armmotorpinF = 4; //forward pin
static int armmotorpinB = 5; //backward pin

static int potpin = A8;

static int lightFrontLeft = A0;
static int lightFrontRight = A11;

static int lightBackCenter = A3;
static int lightBackLeft = A2;

static int switchpin = 22;

static int ledpin = 28;

Robot::Robot() :
		motorright(rmotorpinF, rmotorpinB), motorleft(lmotorpinF, lmotorpinB), motorarm(
				armmotorpinF, armmotorpinB) {
	pinMode(switchpin, INPUT_PULLUP);

	pinMode(ledpin, OUTPUT);

	//turns off the led
	digitalWrite(ledpin, LOW);

	//resets lien count
	count = 0;
	//not on a line
	countstate = false;

	//init driveTillTime
	starttime = millis();

	//position of the pot
	potVval = 244;
	potHval = 0;
}

Robot::~Robot() {
	// TODO Auto-generated destructor stub
}

/**
 * drive the robot for a set amount of time;
 * not interrupt safe
 * does not linetrack
 */
bool Robot::driveTime(int maxspeed, bool forward, int ms) {
	if (millis() - starttime > ms) {
		drive(0, 0);
		return true;
	}
	maxspeed *= forward ? 1 : -1;
	drive(maxspeed, maxspeed);
	return false;
}

/**
 * line track till the limit switch on the alignment is hit
 */
bool Robot::driveTillHit(int avgspeed) {
	if (digitalRead(switchpin) == HIGH) { //means it is not hit
		linetrace(avgspeed);
		return false;
	} else {
		drive(0, 0);
		return true;
	}
}

/**
 * line track till that many lines have been driven passed by the side light tracker
 */
bool Robot::driveTillLine(int linecount, int maxspeed, bool forward) {
	//linear interpolation for speed as the robot approaches the target line speed
	int speedfactor = forward ? 1 : -1; // * (linecount - count) / (linecount);

	//if the turning center is on a line
	if (readlight(lightBackLeft)) {
		//stop if this is the line that is read
		if (count == linecount - 1) {
			drive(0, 0);
			return true;
		}
		countstate = true;
	}
	//increment count when the robot gets off the last line
	if (countstate && !readlight(lightBackLeft)) {
		countstate = false;
		count++;
	}
	linetrace(maxspeed * speedfactor);
	return false;
}


/**
 * line trace using the two light sensors on the alignment
 */
void Robot::linetrace(int maxspeed) {
	double lowfactor = 0.8;
	double highfactor = 1.2;
	// true = on the line (black)
	bool left = readlight(lightFrontLeft);
	bool right = readlight(lightFrontRight);

	//if both detects white or black
	if (left == right) {
		drive(maxspeed, maxspeed);
	}
	//if left detects black
	else if (left) {
		drive(maxspeed * lowfactor, maxspeed * highfactor);
		Serial.println("left.");
	}
	//if right detects black
	else {
		drive(maxspeed * highfactor, maxspeed * lowfactor);
		Serial.println("right.");
	}
}

/**
 * values [-255, 255]
 * positive means forward, and negative means backward
 */
void Robot::drive(int leftspeed, int rightspeed) {
	this->motorleft.drive(leftspeed);
	this->motorright.drive(-rightspeed);
}

/**
 * returns whether the light sensor is on a black line
 */
bool Robot::readlight(int port) {
	//for the line trackers, black -> white = 0 -> 1024
	return analogRead(port) > LIGHT_THRESHOLD;
}

/**
 * reset the states being kept tracked of by the robot
 */
void Robot::resetVals() {
	countstate = false;
	count = 0;
	starttime = millis();
	drive(0, 0);
}

/**
 * drive(0,0) access interface
 */
void Robot::botStop() {
	Serial.println("stop");
	drive(0, 0);
}

/**
 * turns on the led
 */
void Robot::ledOn() {
	digitalWrite(ledpin, HIGH);
}

/**
 * turns off the led
 */
void Robot::ledOFF() {
	digitalWrite(ledpin, LOW);
}

//horizontal = 0
//vertical = 244
//horizontal to vertical, low value -> high value
//pid implemented in another file, but did not have time to integrate
bool Robot::raise4Bar() {
	int pos = analogRead(potpin);
	if (potVval - pos < 10) {
		motorarm.drive(0);
		Serial.println("Raised");
		return true;
	}
	motorarm.drive(-255);
	return false;
}

//high value -> low value
bool Robot::lower4Bar() {
	int pos = analogRead(potpin);
	if (pos - potHval < 10) {
		motorarm.drive(0);
		Serial.println("Lowered");
		return true;
	}
	motorarm.drive(255);
	return false;
}

bool Robot::turn90CCW(int maxspeed, double currentHeading) {
	return turn(90, true, maxspeed, currentHeading);
}

bool Robot::turn90CW(int maxspeed, double currentHeading) {
	return turn(90, false, maxspeed, currentHeading);
}

bool Robot::turn180CCW(int maxspeed, double currentHeading) {
	return turn(180, true, maxspeed, currentHeading);
}

/**
 * This function leaves one light sensor on the black line
 * assuming that the target degree is a multiple of 90, or along the black lines
 * theoretically the fix with a straight movement function call
 */

bool Robot::turn(int degree, bool CCW, int maxspeed, double currentHeading) {
	int buffer = 10;
	if (CCW) {
		if (abs(currentHeading) < (degree - 24)) { //24 degree of buffer, then change to light sensor detection
			drive(-maxspeed, maxspeed);
			Serial.print("rotating: ");
			Serial.println(currentHeading);
		} else {
			if (!readlight(lightFrontLeft)) {
				drive(-0.5 * maxspeed, 0.5 * maxspeed);
				Serial.print("lining: ");
				Serial.println(currentHeading);
			} else {
				drive(0, 0);
				return true;
			}
		}

	} else { //CW
		if (currentHeading < degree - 24) { //24 degree of buffer, then change to light sensor detection
			drive(maxspeed, -maxspeed); //pid control?
			Serial.print("rotating: ");
		} else {
			if (!readlight(lightFrontRight)) {

				Serial.print("lining: ");
				drive(0.5 * maxspeed, -0.5 * maxspeed);
			} else {
				drive(0, 0);
				return true;
			}
		}
	}
	return false;
}

