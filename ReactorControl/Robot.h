/*
 * Robot.h
 * 	Header class/interface for controlling robot parts except for the rack, the gripper, and the encoder
 *
 *  Created on: Feb 24, 2018
 *      Author: Lingrui (Eula)
 */

#ifndef ROBOT_H_
#define ROBOT_H_

#include "PololuMotor.h"

class Robot {
	typedef enum {
		A, B, ST1, ST2, ST3, ST4, //storage
		SP1,
		SP2,
		SP3,
		SP4	//supplies
	} location;

public:
	Robot();

	virtual ~Robot();

	bool driveTillHit(int maxspeed);

	bool turn90CCW(int maxspeed, double currentHeading);

	bool turn90CW(int maxspeed, double currentHeading);

	bool turn180CCW(int maxspeed, double currentHeading);

	/**
	 * uses the back left back light sensor
	 */
	bool driveTillLine(int linecount, int maxspeed, bool forward);

	bool raise4Bar();

	bool lower4Bar();

	/**
	 * in milliseconds
	 */
	bool driveTime(int maxspeed, bool forward, int ms);

	void resetVals();

	void botStop();

	void ledOn();

	void ledOFF();

private:
	PololuMotor motorright;
	PololuMotor motorleft;

	PololuMotor motorarm;

	int count;

	bool countstate;

	long starttime;

	long potVval;

	long potHval;

	void drive(int leftspeed, int rightspeed);
	void linetrace(int avgspeed);
	/**
	 * check if the light sensor is considered white or dark
	 * returns true if on the line
	 */
	bool readlight(int port);

	bool turn(int degree, bool CCW, int maxspeed, double currentHeading);
};

#endif /* ROBOT_H_ */
