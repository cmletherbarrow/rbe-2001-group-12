/*
 * PololuMotor.cpp
 *
 *  Created on: Feb 24, 2018
 *      Author: Lingrui
 */

#include "PololuMotor.h"
#include <Arduino.h>

//PololuMotor::PololuMotor(int forward, int backward)
//:forwardPin(forward), backwardPin(backward){
//
//	// TODO Auto-generated constructor stub
//
//	pinMode(forwardPin, OUTPUT);
//	pinMode(backwardPin, OUTPUT);
//}

PololuMotor::PololuMotor(){
	forwardPin = 0;
	backwardPin = 0;
}

void PololuMotor::init(int forward, int backward){
	this->forwardPin = forward;
	this->backwardPin = backward;
}


void PololuMotor::drive(int value){
	if (value > 255)	value = 255;
	if (value < 255)	value = -255;
	if (value >= 0){
		analogWrite(forwardPin, value);
		analogWrite(backwardPin, 0);
	}else{
		analogWrite(forwardPin, 0);
		analogWrite(backwardPin, value);
	}
}

PololuMotor::~PololuMotor() {
	// TODO Auto-generated destructor stub
}

