/*
 * PololuMotor.h
 *
 *  Created on: Feb 24, 2018
 *      Author: Lingrui
 */

#ifndef POLOLUMOTOR_H_
#define POLOLUMOTOR_H_

class PololuMotor {
public:
	PololuMotor();
//	PololuMotor(int forward, int backward);
	void init(int forward, int backward);
	virtual ~PololuMotor();

	/**
	 * value: [-255,255] with 0 being stop. Positive means turning clockwise
	 */
	void drive(int value);
private:
	int forwardPin;
	int backwardPin;
};

#endif /* POLOLUMOTOR_H_ */
