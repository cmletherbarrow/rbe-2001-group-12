#include <Arduino.h>
#include "Messages.h"
#include <Servo.h>
#include "Robot.h"

Messages msg;
Servo motor;
unsigned long timeForHeartbeat;
int pin = 22;
Robot robot;

typedef enum  {
	PickingRodFromA,
	AToStorage,
	StorageToSupply,
	SupplyToReactor,
	DispensingAtA,
	EStop
} largeState;

largeState largestate;

largeState previousLS;//previous large state; used for emergency stop

//each corresponds to a function in Robot.h
typedef enum {
	GetRodV,
	GetRodH,
	DispenseRodV,
	DispenseRodH,
	Raise4Bar,
	Lower4Bar,
	DriveTillLine,
	TurnCCW90,
	TurnCW90,
	Turn180,
	DriveTime,
	DriveTillHit
} smallState;

smallState smallstate;

bool availabilityST[4];

bool availabilitySP[4];

location closestA_ST; // closest ST slot from A

location closestST_SP [4]; // closest SP slot from each ST

void pickingRodFromA(){
	switch(smallstate){
	case GetRodV:
		if (robot.getRodV()){
			smallstate = Raise4Bar;
		}
		break;
	case Raise4Bar:
		if (robot.raise4Bar()){
			smallstate = DriveTillLine;
		}
		break;
	case DriveTime://back up
		if (robot.driveTime(500, 45)){
			smallstate = Turn180;
		}
		break;
	case Turn180:
		if (robot.turn180(45)){
			largestate = AToStorage;
			smallstate = DriveTillLine;
		}
		break;
	}

}

void reactorToStorage(){
	//set src and dest
//	robot.setSrcDest(A, ST1);
	switch(smallstate){
	case DriveTillLine:
		if (robot.driveTillLine(closestA_ST - 1, 90, false)){//drive till the back senses the line
			smallstate = TurnCCW90;
		}
		break;
	case TurnCCW90:
		if (robot.turn90(true, 45)){
			smallstate = DriveTillHit;
		}
		break;
	case DriveTillHit:
		if (robot.driveTillHit(90)){
			smallstate = DispenseRodH;
		}
		break;
	case DispenseRodH:
		if (robot.dispenseRodV()){
			largestate = StorageToSupply;
		}
		break;
	}
}

void storageToSupply(){
//three cases switch case
//set src and dest
}

void supplyToReactor(){
//set src and dest
	switch(smallstate){
	case DriveTillLine://backup
		break;
	case TurnCW90:
		break;
	case DriveTillHit:
		break;
	}
}

void dispensingAtA(){
	switch(smallstate){
		case Lower4Bar:
			break;
		case DispenseRodV:
			break;
	}
}

void calcClosestSP(){
	int closest;
	for (int x = 0; x < 4; x++){
		if (availabilitySP[3-x]){
			closest = 3-x;
		}else{
			for (int y = 0; y < 4; y++){//lower the number, closer to A
				if (availabilitySP[y]){
					closest = y;
					break;
				}
			}
		}
		switch(closest){
		case 0: closestST_SP = SP1;
			break;
		case 1: closestST_SP = SP2;
			break;
		case 2: closestST_SP = SP3;
			break;
		case 3: closestST_SP = SP4;
			break;
		}
	}
}

void setSupply(bool avai[4]){
	for (int x = 0; x < 4; x++){
		if (avai[x] != availabilitySP[x]){
			for (int y = 0; y < 4; y++){
				availabilitySP[y] = avai[y];
			}
			calcClosestSP();
			return;
		}
	}
}

void calcClosestST(){
	for (int x = 3; x >= 0; x--){//higher the number, closer to A
		if (availabilityST[x]){
			switch(x){
			case 0: closestA_ST = ST1;
				break;
			case 1: closestA_ST = ST2;
				break;
			case 2: closestA_ST = ST3;
				break;
			case 3: closestA_ST = ST4;
				break;
			}
			return;
		}
	}

}

void setStorage(bool avai[4]){
	for (int x = 0; x < 4; x++){
		if (avai[x] != availabilityST[x]){
			for (int y = 0; y < 4; y++){
				availabilityST[y] = avai[y];
			}
			calcClosestST();
			return;
		}
	}
}





/**
 * Initialize the messages class and the debug serial port
 */
void setup() {
	Robot robot();
	largestate = PickingRodFromA;
	smallstate = GetRodV;
//  Serial.begin(115200);
//  Serial.println("Starting");
//  motor.attach(pin);
	msg.setup();
//  timeForHeartbeat = millis() + 1000;
}

/**
 * The loop method runs continuously, so call the Messages read method to check for input
 * on the bluetooth serial port and parse any complete messages. Then send the heartbeat if
 * it's time to do that.
 *
 * For the final project, one good way of implementing it is to use a state machine with high
 * level tasks as states. The state will tell what you should be doing each time the loop
 * function is called.
 *
 * Refer to the state machine lecture or look at the BTComms class for an example on how to
 * implement state machines.
 */
void loop() {
	switch(largestate){
	case PickingRodFromA:
		pickingRodFromA();
		break;
	case AToStorage:
		reactorToStorage();
		break;
	case StorageToSupply:
		storageToSupply();
		break;
	case SupplyToReactor:
		supplyToReactor();
		break;
	case DispensingAtA:
		dispensingAtA();
		break;
	case EStop:
		break;
	}

//  if (msg.read()) {
//	  msg.printMessage();
//  }
//  if (msg.isStopped()){
//    motor.write(90);
//    Serial.println("Stopped");
//  }
//  else {
//    motor.write(0);
//    Serial.println("Running");
//  }
//  if (millis() > timeForHeartbeat) {
//    timeForHeartbeat = millis() + 1000;
//    msg.sendHeartbeat();
//  }
}