#include "Robot.h"
#include <Arduino.h>

static int rmotorpinF = 2;//forward pin
static int rmotorpinB = 2;//backward pin

static int lmotorpinF = 2;//forward pin
static int lmotorpinB = 3;//backward pin
//
//static int lencoderpin1 = 4;
//static int lencoderpin2 = 6;
//
//static int armmotorpin = 14;
//
//static int rackservopin = 15;
//
//static int rencoderpin1 = 5;
//static int rencoderpin2 = 7;
//
static int lightFrontLeft = A8;
static int lightFrontRight = A9;
//static int lightBackCenter = A10;
//static int lightBackRight = A11;
//
static int buttonpin = 12;
//
//static int potpin = 13;

Robot::Robot(){
//	this->lineCount = 0;
//	this->starttime = 0;
//	this->DIRECTION = 0;
//	this->DEST = 0;
//	this->SRC = 0;

//	(this->motorLeft)(lmotorpinF, lmotorpinB);
//	(this->motorRight)(rmotorpinF, rmotorpinB);
	this->motorLeft.init(lmotorpinF, lmotorpinB);
	this->motorRight.init(rmotorpinF, rmotorpinB);

	pinMode(buttonpin, INPUT);
	pinMode(lightFrontLeft, INPUT);
	pinMode(lightFrontRight, INPUT);

	//Serial.begin(9600);
}

bool Robot::driveTillHit(int speed){
	if (digitalRead(buttonpin) == HIGH){
		this->drive(0);
		return true;
	}else{
		this->drive(speed);
		return false;
	}
}

void Robot::drive(int speed){
	motorLeft.drive(speed);
	motorRight.drive(-speed);
}


