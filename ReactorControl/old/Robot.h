#ifndef ROBOT_H_
#define ROBOT_H_
//for every control function in here, returns a boolean that indicate whether the action has finished

//#include <Arduino.h>
#include "PololuMotor.h"

typedef enum{
	A, B,
	ST1, ST2, ST3, ST4, //storage
	SP1, SP2, SP3, SP4	//supplies
} location;

//typedef enum{
//	A, B, ST, SP
//} direction;

class Robot{
public:
	Robot();
  bool driveTillHit(int avgspeed);

//	void setSrcDest(location src, location dest);
//	bool getRodV();
//	bool getRodH();
//	bool dispenseRodV();
//	bool raise4Bar();
//	bool lower4Bar();
//	bool driveTillLine(int linecount, int speed, bool front);//how many lines till stop; negative speed = backward; should it use the front sensor to detect or the back
//	bool turn90(bool CCW, int speed); //true if CCW
//	bool turn180(int speed);
//	bool driveTime(int millis, int speed);//duration in
//	void stop();


private:
//	location SRC, DEST;
//	direction DIRECTION;
//	int lineCount;
//	void resetcounts();//should be called when every movement function returns true
//	int starttime;

	PololuMotor motorRight;
	PololuMotor motorLeft;

	/**
	 * value: [-255, 255] 255 being robot moving forward
	 */
	void drive(int speed);
};

#endif
