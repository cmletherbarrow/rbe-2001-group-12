#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-03-02 22:44:02

#include "Arduino.h"
#include "Arduino.h"
#include "Robot.h"
#include "Messages.h"
#include "EStop.h"
#include "L3G.h"
#include "Wire.h"
#include "Servo.h"
#include "Encoder.h"

void zeroHeading() ;
double getHeading() ;
void startrack() ;
bool retractRack(int ticks) ;
bool extendRack(int ticks) ;
void startgripper() ;
bool closeGripper() ;
bool openGripper() ;
void findClosestStorageB() ;
void findClosestStorageA() ;
void closestSupplyFromStorage(int st) ;
void pickingRod() ;
void reactorToStorage() ;
bool STSright() ;
bool STSleft() ;
void storageToSupply() ;
void supplyToReactor() ;
void aToB() ;
void eStop() ;
void setup() ;
void loop() ;

#include "ReactorControl.ino"


#endif
